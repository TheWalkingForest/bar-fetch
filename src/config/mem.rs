#[derive(Debug, Serialize, Deserialize)]
pub struct MemConfig {
    pub enabled: bool,
    pub format: String,
}

impl Default for MemConfig {
    fn default() -> Self {
        MemConfig {
            enabled: true,
            format: "%u".to_owned(),
        }
    }
}
