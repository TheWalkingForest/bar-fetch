pub mod command;
pub mod cpu;
pub mod date;
pub mod fs;
pub mod mem;

use std::collections::HashMap;

use crate::config::cpu::CPUConfig;

use self::{command::CommandConfig, date::DateConfig, fs::FSConfig, mem::MemConfig};

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub cpu: CPUConfig,
    pub date: DateConfig,
    pub fs: FSConfig,
    pub mem: MemConfig,
    pub command: Vec<CommandConfig>,
}

impl Config {
    pub fn parse() -> Config {
        let cpu: HashMap<String, String>;
        let date: HashMap<String, String>;
        let fs: HashMap<String, String>;
        let mem: HashMap<String, String>;
        let command: Vec<CommandConfig> = Vec::new();

        Config {
            cpu: CPUConfig::default(),
            date: DateConfig::default(),
            fs: FSConfig::default(),
            mem: MemConfig::default(),
            command,
        }
    }
}
