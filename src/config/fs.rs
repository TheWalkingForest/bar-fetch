#[derive(Debug, Serialize, Deserialize)]
pub struct FSConfig {
    pub enabled: bool,
    pub format: String,
}

impl Default for FSConfig {
    fn default() -> Self {
        FSConfig {
            enabled: true,
            format: "%u".to_owned(),
        }
    }
}
