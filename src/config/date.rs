#[derive(Debug, Serialize, Deserialize)]
pub struct DateConfig {
    pub enabled: bool,
    pub format: String,
}

impl Default for DateConfig {
    fn default() -> Self {
        DateConfig {
            enabled: true,
            format: "%D %R".to_owned(),
        }
    }
}
