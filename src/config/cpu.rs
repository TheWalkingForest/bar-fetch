#[derive(Debug, Serialize, Deserialize)]
pub struct CPUConfig {
    pub enabled: bool,
    pub format: String,
}

impl Default for CPUConfig {
    fn default() -> Self {
        CPUConfig {
            enabled: true,
            format: "%u".to_owned(),
        }
    }
}
