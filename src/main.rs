mod config;
mod cpu;
mod date;
mod fs;
mod mem;

#[macro_use]
extern crate serde_derive;
extern crate toml;

use std::collections::HashMap;

use serde::Deserialize;

fn main() {
    let config: Config = toml::from_str(&toml_string).unwrap();
    println!("{:?}", config);
}
